----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:22:24 05/10/2019 
-- Design Name: 
-- Module Name:    Banc_de_Registres - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Banc_de_Registres is
    Port ( 
			  CLK  : in   STD_LOGIC;	 
           W    : in   STD_LOGIC; -- Actif à 1
			  
           ATW   : in   STD_LOGIC_VECTOR(7 downto 0); --\
			  DATA : in   STD_LOGIC_VECTOR(7 downto 0); --- Ecriture dans un registre
			  
			  ATA   : in   STD_LOGIC_VECTOR(7 downto 0); --\
			  QA   : out  STD_LOGIC_VECTOR(7 downto 0); --- Lecture du premier registre
			  
			  ATB   : in   STD_LOGIC_VECTOR(7 downto 0); --\
			  QB   : out  STD_LOGIC_VECTOR(7 downto 0); --- Lecture du second registre
			  
			  RST  : in   STD_LOGIC; -- Actif à 0
			  
			  --Banc de registre
			  R1 : out STD_LOGIC_VECTOR(7 downto 0);
			  R2 : out STD_LOGIC_VECTOR(7 downto 0)
			  
			 );
			 
end Banc_de_Registres;

architecture Behavioral of Banc_de_Registres is

	type registre_tab is array(0 to 15) of STD_LOGIC_VECTOR(7 downto 0);
	signal reg : registre_tab := (others => x"00");	
	
begin
	
	reg_clk : process
	begin
		wait until CLK'event and CLK='1';
		--write
		if W = '1' then reg(to_integer(unsigned(ATW))) <= DATA;
		end if;
		--rst
		if (RST = '0') then 
			for I in 0 to 15 loop 
				reg(I) <= x"00";
			end loop;
		end if;
		
	end process;
	
	--read
	QA <= reg(to_integer(unsigned(ATA))) when (ATA < x"0F");
	QB <= reg(to_integer(unsigned(ATB))) when (ATB < x"0F");
	R1 <= reg(1);
	R2 <= reg(2);


end Behavioral;

