----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:25:14 05/17/2019 
-- Design Name: 
-- Module Name:    CPU - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CPU is
    Port ( CLK   : in   STD_LOGIC;
	        -- Interface ROM
			  INSTR : in   STD_LOGIC_VECTOR(31 downto 0);
			  INS_A : out STD_LOGIC_VECTOR(15 downto 0);
			  -- Interface RAM
			  DATA_DI  : in  STD_LOGIC_VECTOR(7 downto 0);
			  DATA_WE  : out  STD_LOGIC;
			  DATA_A  : out  STD_LOGIC_VECTOR(7 downto 0);
			  DATA_DO  : out  STD_LOGIC_VECTOR(7 downto 0);
			  --TEST AFC
			  R1 : out STD_LOGIC_VECTOR(7 downto 0);
			  R2 : out STD_LOGIC_VECTOR(7 downto 0);
			  TEST: out STD_LOGIC_VECTOR(7 downto 0));
end CPU;

architecture Structural of CPU is
	--COMPONENTS
	----Decode
	component Decode is
    Port ( INPUT : in   STD_LOGIC_VECTOR(31 downto 0);
           OP    : out  STD_LOGIC_VECTOR(7  downto 0);
           A     : out  STD_LOGIC_VECTOR(7 downto 0);
           B     : out  STD_LOGIC_VECTOR(7 downto 0);
           C     : out  STD_LOGIC_VECTOR(7 downto 0));
	end component;
	
	----Pipeline
	component Pipeline is
		port ( CLK      : in  STD_LOGIC;
			 OP_IN  : in  STD_LOGIC_VECTOR(7  downto 0);
			 OP_OUT : out STD_LOGIC_VECTOR(7  downto 0);
			 A_IN   : in  STD_LOGIC_VECTOR(7 downto 0);
			 A_OUT  : out STD_LOGIC_VECTOR(7 downto 0);
			 B_IN   : in  STD_LOGIC_VECTOR(7 downto 0);
			 B_OUT  : out STD_LOGIC_VECTOR(7 downto 0);
			 C_IN   : in  STD_LOGIC_VECTOR(7 downto 0);
			 C_OUT  : out STD_LOGIC_VECTOR(7 downto 0)	 
		   );
	end component;
	
	----Banc de Registre
	component Banc_de_Registres is
		Port ( 
			  CLK  : in   STD_LOGIC;	 
           W    : in   STD_LOGIC; -- Actif à 1			  
           ATW  : in   STD_LOGIC_VECTOR(7 downto 0); --\
			  DATA : in   STD_LOGIC_VECTOR(7 downto 0); --- Ecriture dans un registre
			  ATA  : in   STD_LOGIC_VECTOR(7 downto 0); --\
			  QA   : out  STD_LOGIC_VECTOR(7 downto 0); --- Lecture du premier registre
			  ATB  : in   STD_LOGIC_VECTOR(7 downto 0); --\
			  QB   : out  STD_LOGIC_VECTOR(7 downto 0); --- Lecture du second registre
			  RST  : in   STD_LOGIC; -- Actif à 0
			  R1 : out STD_LOGIC_VECTOR(7 downto 0);
			  R2 : out STD_LOGIC_VECTOR(7 downto 0)
			 );	 
	end component;
	
	----ALU
	component ALU is
		Port ( A    : in   STD_LOGIC_VECTOR(7 downto 0);
           B    : in   STD_LOGIC_VECTOR(7 downto 0);
           OP   : in   STD_LOGIC_VECTOR(7 downto 0);
           S    : out  STD_LOGIC_VECTOR(7 downto 0);
           FLAG : out  STD_LOGIC_VECTOR(3 downto 0)
			  );
	end component;
	
	----LC
	component LC is
		Generic ( TL : natural);
		Port ( OP : in  STD_LOGIC_VECTOR(7 downto 0);
             W  : out  STD_LOGIC);
	end component;
	
	----MUX
	component MUX is
	Generic ( TM : natural);
		Port ( OP : in  STD_LOGIC_VECTOR(7 downto 0);
           IN_1 : in  STD_LOGIC_VECTOR(7 downto 0);
           IN_2   : in  STD_LOGIC_VECTOR(7 downto 0);
           OUTPUT: out  STD_LOGIC_VECTOR(7 downto 0));
	end component;

	--IP
	component IP is
    Port ( INIT : in  STD_LOGIC_VECTOR(15 downto 0);
           CLK : in  STD_LOGIC;
           W : in  STD_LOGIC;
           INS_A : out  STD_LOGIC_VECTOR(15 downto 0));
	end component;
	
	--signals
	signal  LI_OP,  LI_A,  LI_B,  LI_C,
	        DI_OP,  DI_A,  DI_B, DI_C,
			  EX_OP,  EX_A,  EX_B,  EX_C,
			 MEM_OP, MEM_A, MEM_B, MEM_C,
			  RE_OP,  RE_A,  RE_B,  RE_C, 
			  QA, QB, ALU_OUT,
			  MUX_EX_OUT, MUX_RE_OUT, MUX_DI_OUT, MUX_MEM_OUT: STD_LOGIC_VECTOR(7 downto 0);
	
	signal W_OP : STD_LOGIC;
	
begin

	--ETAGE LI
	IPoint : IP port map (x"0000", CLK, '0', INS_A);
	Dec : Decode port map (INSTR, LI_OP, LI_A, LI_B, LI_C);
	
	--Pipeline LI/DI
	P_LI_DI : Pipeline port map (CLK, LI_OP, DI_OP, LI_A, DI_A, LI_B, DI_B, LI_C, DI_C);
	
	--ETAGE DI
	Reg : Banc_de_Registres port map (CLK, W_OP, RE_A, MUX_RE_OUT, DI_B, QA, DI_C, QB, '1', R1, R2);
	MUX_DI : MUX generic map (1) port map (DI_OP, DI_B, QA, MUX_DI_OUT);
	
	--Pipeline DI/EX
	P_DI_EX : Pipeline port map (CLK, DI_OP, EX_OP, DI_A, EX_A, MUX_DI_OUT, EX_B, QB, EX_C);
	
	--ETAGE EX
	UAL : ALU port map (EX_B, EX_C, EX_OP, ALU_OUT, open);
	MUX_EX : MUX  generic map (2) port map (EX_OP, EX_B, ALU_OUT, MUX_EX_OUT); 
	
	--Pipeline EX/Mem
	P_EX_Mem : Pipeline port map (CLK, EX_OP, MEM_OP, EX_A, MEM_A, MUX_EX_OUT, MEM_B, x"00", open);

	--ETAGE Mem
	MEM_LC : LC generic map (1) port map (MEM_OP, DATA_WE);
	MUX_MEM : MUX  generic map (3) port map (MEM_OP, MEM_A, MEM_B, MUX_MEM_OUT);
	
	--Pipeline Mem/RE
	P_Mem_RE : Pipeline port map (CLK, MEM_OP, RE_OP, MEM_A, RE_A, MEM_B, RE_B, x"00", open);

	--ETAGE RE
	RE_LC : LC generic map (2) port map (RE_OP, W_OP);
	MUX_RE : MUX  generic map (4) port map (RE_OP, RE_B, DATA_DI, MUX_RE_OUT);





	--OUTPUT :
	DATA_A <= MUX_MEM_OUT;
	DATA_DO <= MEM_B;
	
	
	--TEST :
	TEST <= DI_B;
end Structural;

