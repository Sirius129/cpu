----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:01:54 04/18/2019 
-- Design Name: 
-- Module Name:    ALU - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALU is
    Port ( A    : in   STD_LOGIC_VECTOR(7 downto 0);
           B    : in   STD_LOGIC_VECTOR(7 downto 0);
           OP   : in   STD_LOGIC_VECTOR(7 downto 0);
           S    : out  STD_LOGIC_VECTOR(7 downto 0);
           FLAG : out  STD_LOGIC_VECTOR(3 downto 0)
			  );
end ALU;

architecture Behavioral of ALU is

	--Signal de calcul :
	signal S_add:STD_LOGIC_VECTOR(8 downto 0);  --ADD <= x"1"
	signal S_mul:STD_LOGIC_VECTOR(15 downto 0); --MUL <= x"2"
	signal S_sub:STD_LOGIC_VECTOR(8 downto 0);  --SOU <= x"3"
	signal S_div:STD_LOGIC_VECTOR(16 downto 0); --DIV <= x"4"

	--Signal de traitement :
	signal S_treatment:STD_LOGIC_VECTOR(7 downto 0);
	
	--Flags :
	---- FLAG := [C;N;Z;O]
	---- C : Carry
	---- N : Negative
	---- Z : Zero
	---- O : Overflow

begin
	
	--Calcul des sorties possibles :
	S_add <= ('0'&A)+('0'&B); --ADD
	S_mul <= A*B;             --MUL
	S_sub <= ('0'&A)-('0'&B); --SOU
	--S_div <= A/B;
	
	--Selection de la sortie :
	S_treatment <= S_add(7 downto 0) when OP = x"01" else --ADD
		            S_mul(7 downto 0) when OP = x"02" else --MUL
	               S_sub(7 downto 0) when OP = x"03" ;    --SOU
						
	--Mise en sortie !
	S <= S_treatment;


	--Levage de FLAG :
	---- C :
	FLAG(3) <= S_add(8) when OP = x"01" else --ADD
	           S_sub(8) when OP = x"03" ;    --SOU
	---- N :
	FLAG(2) <= S_treatment(7); --GENERAL CASE
	
	---- Z :
	FLAG(1) <= '1' when S_treatment = x"00" else '0'; --GENERAL CASE
	
	---- O :
	FLAG(0) <= '1' when A(7)=B(7) and A(7)=not(S_add(7)) and OP = x"01" else --ADD
              '1' when S_mul(15 downto 8) /=  x"00"     and OP = x"02" else --MUL
	           '1' when A(7)=not(B(7)) and B(7)=S_mul(7) and OP = x"03" else --SOU
				  '0';

end Behavioral;

