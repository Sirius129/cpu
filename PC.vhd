----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:01:39 05/27/2019 
-- Design Name: 
-- Module Name:    PC - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PC is
    Port ( CLK : in  STD_LOGIC;
           INSTROUT : out  STD_LOGIC_VECTOR(31 downto 0);
           DATA : out  STD_LOGIC_VECTOR(7 downto 0));
end PC;

architecture Structural of PC is
	--COMPONENTS
	--CPU
	component CPU is 
	Port ( CLK   : in   STD_LOGIC;
	        -- Interface ROM
			  INSTR : in   STD_LOGIC_VECTOR(31 downto 0);
			  INS_A : out STD_LOGIC_VECTOR(15 downto 0);
			  -- Interface RAM
			  DATA_DI  : in  STD_LOGIC_VECTOR(7 downto 0);
			  DATA_WE  : out  STD_LOGIC;
			  DATA_A  : out  STD_LOGIC_VECTOR(7 downto 0);
			  DATA_DO  : out  STD_LOGIC_VECTOR(7 downto 0));
	end component;
	--ROM
	component instr_memory is
	Generic(LEN_SEL: natural := 16;
		     LEN_INSTR: natural := 32);
	Port(sel: in std_logic_vector(LEN_SEL-1 downto 0);
		  q: out std_logic_vector(LEN_INSTR-1 downto 0));
	end component;
	--RAM
	component RAM is
	Port ( ADDR : in  STD_LOGIC_VECTOR(7 downto 0);
           RAM_IN : in  STD_LOGIC_VECTOR(7 downto 0);
           RW : in  STD_LOGIC;
           RST : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           RAM_OUT : out  STD_LOGIC_VECTOR(7 downto 0));
	end component;
	
	signal INSTR: STD_LOGIC_VECTOR(31 downto 0);
	signal INS_A: STD_LOGIC_VECTOR(15 downto 0);
	signal DATA_DI, DATA_A, DATA_DO: STD_LOGIC_VECTOR(7 downto 0);
	signal DATA_WE: STD_LOGIC;
	
begin
	--CPU
	Processeur	: CPU port map (CLK, INSTR, INS_A, DATA_DI, DATA_WE, DATA_A, DATA_DO);
	
	--RAM
	MemoryBench : RAM port map (DATA_A, DATA_DO, DATA_WE, '0', CLK, DATA_DI);
	
	--ROM
	MemoryInstr : instr_memory port map(INS_A, INSTR);

	INSTROUT <= INSTR;
	DATA <= DATA_DO;
	
	
end Structural;

