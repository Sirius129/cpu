----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:29:11 05/20/2019 
-- Design Name: 
-- Module Name:    MUX - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUX is
	 Generic ( TM : natural);
    Port ( OP : in  STD_LOGIC_VECTOR(7 downto 0);
           IN_1 : in  STD_LOGIC_VECTOR(7 downto 0);
           IN_2 : in  STD_LOGIC_VECTOR(7 downto 0);
           OUTPUT : out  STD_LOGIC_VECTOR(7 downto 0));
end MUX;

architecture Behavioral of MUX is

begin
	--DI MUX
	OUTPUT <= IN_2 when ((OP = x"01" --ADD
				 or  OP = x"02" --MUL 
				 or  OP = x"03" --SOU
				 or  OP = x"04" --DIV
	  		    or  OP = x"05" --COP
				 or  OP = x"08" --STORE
				 or  OP = x"09" --EQU
				 or  OP = x"0A" --INF
				 or  OP = x"0B" --INFE
				 or  OP = x"0C" --SUP
				 or  OP = x"0D" --SUPE
				 or  OP = x"0F" --JMPC
				 ) and TM = 1)
	else IN_1 when (TM = 1)
	
	--EX MUX
	else IN_2 when ((OP = x"01" --ADD
				 or  OP = x"02" --MUL 
				 or  OP = x"03" --SOU
				 or  OP = x"04" --DIV
				 or  OP = x"09" --EQU
				 or  OP = x"0A" --INF
				 or  OP = x"0B" --INFE
				 or  OP = x"0C" --SUP
				 or  OP = x"0D" --SUPE
				 ) and TM = 2)
	else IN_1 when (TM = 2)		
	
	--MEM MUX
	else IN_1 when (OP = x"08" --STORE 
				  and TM = 3)
	else IN_2 when (TM = 3)
	
	--RE MUX
	else IN_2 when (OP = x"07" --LOAD
				  and TM = 4)
	else IN_1 when (TM = 4);
	
end Behavioral;

