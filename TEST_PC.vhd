--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:59:04 05/29/2019
-- Design Name:   
-- Module Name:   /home/tallaa/Documents/Parser/projet_cpu/TEST_PC.vhd
-- Project Name:  CPU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: PC
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TEST_PC IS
END TEST_PC;
 
ARCHITECTURE behavior OF TEST_PC IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT PC
    PORT(
         INSTRIN : IN  std_logic_vector(31 downto 0);
         CLK : IN  std_logic;
         INSTROUT : OUT  std_logic_vector(31 downto 0);
         DATA : OUT  std_logic_vector(7 downto 0);
         R1 : OUT  std_logic_vector(7 downto 0);
         R2 : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal INSTRIN : std_logic_vector(31 downto 0) := (others => '0');
   signal CLK : std_logic := '0';

 	--Outputs
   signal INSTROUT : std_logic_vector(31 downto 0);
   signal DATA : std_logic_vector(7 downto 0);
   signal R1 : std_logic_vector(7 downto 0);
   signal R2 : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: PC PORT MAP (
          INSTRIN => INSTRIN,
          CLK => CLK,
          INSTROUT => INSTROUT,
          DATA => DATA,
          R1 => R1,
          R2 => R2
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus 
	INSTRIN <= x"06010300";

END;
