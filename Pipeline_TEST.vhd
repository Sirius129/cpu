--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:16:07 05/14/2019
-- Design Name:   
-- Module Name:   /home/charreir/Documents/4aIR/S2/CompiloEtCPU/vhdl/CPU/CPU/Pipeline_TEST.vhd
-- Project Name:  CPU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Pipeline
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Pipeline_TEST IS
END Pipeline_TEST;
 
ARCHITECTURE behavior OF Pipeline_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Pipeline
    PORT(
         CLK : IN  std_logic;
         OP_IN : IN  std_logic_vector(7 downto 0);
         OP_OUT : OUT  std_logic_vector(7 downto 0);
         A_IN : IN  std_logic_vector(7 downto 0);
         A_OUT : OUT  std_logic_vector(7 downto 0);
         B_IN : IN  std_logic_vector(7 downto 0);
         B_OUT : OUT  std_logic_vector(7 downto 0);
         C_IN : IN  std_logic_vector(7 downto 0);
         C_OUT : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal OP_IN : std_logic_vector(7 downto 0) := (others => '0');
   signal OP_OUT : std_logic_vector(7 downto 0) := (others => '0');
   signal A_IN : std_logic_vector(7 downto 0) := (others => '0');
   signal B_IN : std_logic_vector(7 downto 0) := (others => '0');
   signal C_IN : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal A_OUT : std_logic_vector(7 downto 0);
   signal B_OUT : std_logic_vector(7 downto 0);
   signal C_OUT : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 100 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Pipeline PORT MAP (
          CLK => CLK,
          OP_IN => OP_IN,
          OP_OUT => OP_OUT,
          A_IN => A_IN,
          A_OUT => A_OUT,
          B_IN => B_IN,
          B_OUT => B_OUT,
          C_IN => C_IN,
          C_OUT => C_OUT
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 
   -- Stimulus 
	--A_IN <= x"0001" after 50 ns, x"0002" after 150 ns, x"0003" after 250 ns, x"0000" after 260 ns;

END;
