----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:26:14 05/17/2019 
-- Design Name: 
-- Module Name:    Decode - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Decode is
    Port ( INPUT : in   STD_LOGIC_VECTOR(31 downto 0);
           OP    : out  STD_LOGIC_VECTOR(7  downto 0);
           A     : out  STD_LOGIC_VECTOR(7 downto 0);
           B     : out  STD_LOGIC_VECTOR(7 downto 0);
           C     : out  STD_LOGIC_VECTOR(7 downto 0));
end Decode;

architecture Behavioral of Decode is

begin

	--OPERATION
	OP <= INPUT(31 downto 24);
	--A
	A  <= INPUT(23 downto 16);
	--B
	B  <= INPUT(15 downto 8);
	--C
	C  <= INPUT(7  downto 0);

end Behavioral;

