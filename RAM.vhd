----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:13:50 05/17/2019 
-- Design Name: 
-- Module Name:    RAM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RAM is
    Port ( ADDR : in  STD_LOGIC_VECTOR(7 downto 0);
           RAM_IN : in  STD_LOGIC_VECTOR(7 downto 0);
           RW : in  STD_LOGIC;
           RST : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           RAM_OUT : out  STD_LOGIC_VECTOR(7 downto 0) );
end RAM;

architecture Behavioral of RAM is

	type memory is array(0 to 255) of STD_LOGIC_VECTOR(7 downto 0);
	signal mem : memory := (others => x"00");	

begin

	RAM : process
	begin 
		wait until CLK'event and CLK='1';
		if (RW = '1') then --Lecture
			RAM_OUT <= mem(to_integer(unsigned(ADDR)));
		end if;
		if (RW = '0') then --Ecriture
			mem(to_integer(unsigned(ADDR))) <= RAM_IN;
		end if;
		if (RST = '1') then
			for I in 0 to 254 loop mem(I) <= x"00";
			end loop;
		end if;
	end process;

end Behavioral;

