--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:36:59 05/29/2019
-- Design Name:   
-- Module Name:   /home/tallaa/Documents/Parser/projet_cpu/CPU_TEST.vhd
-- Project Name:  CPU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: CPU
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY CPU_TEST IS
END CPU_TEST;
 
ARCHITECTURE behavior OF CPU_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT CPU
    PORT(
         CLK : IN  std_logic;
         INSTR : IN  std_logic_vector(31 downto 0);
         INS_A : OUT  std_logic_vector(15 downto 0);
         DATA_DI : IN  std_logic_vector(7 downto 0);
         DATA_WE : OUT  std_logic;
         DATA_A : OUT  std_logic_vector(7 downto 0);
         DATA_DO : OUT  std_logic_vector(7 downto 0);
         R1 : OUT  std_logic_vector(7 downto 0);
         R2 : OUT  std_logic_vector(7 downto 0);
         TEST : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal INSTR : std_logic_vector(31 downto 0) := (others => '0');
   signal DATA_DI : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal INS_A : std_logic_vector(15 downto 0);
   signal DATA_WE : std_logic;
   signal DATA_A : std_logic_vector(7 downto 0);
   signal DATA_DO : std_logic_vector(7 downto 0);
   signal R1 : std_logic_vector(7 downto 0);
   signal R2 : std_logic_vector(7 downto 0);
   signal TEST : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: CPU PORT MAP (
          CLK => CLK,
          INSTR => INSTR,
          INS_A => INS_A,
          DATA_DI => DATA_DI,
          DATA_WE => DATA_WE,
          DATA_A => DATA_A,
          DATA_DO => DATA_DO,
          R1 => R1,
          R2 => R2,
          TEST => TEST
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus 
	INSTR <= x"06010303", x"06020100" after 200 ns, x"01030201" after 400 ns;
END;
