----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:43:10 05/27/2019 
-- Design Name: 
-- Module Name:    IP - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IP is
    Port ( INIT : in  STD_LOGIC_VECTOR(15 downto 0);
           CLK : in  STD_LOGIC;
           W : in  STD_LOGIC;
           INS_A : out  STD_LOGIC_VECTOR(15 downto 0));
end IP;

architecture Behavioral of IP is
	signal save : std_logic_vector(15 downto 0):=x"0000";
begin
	IP : process
	begin
		wait until CLK'event and CLK='1';
		if (W='1') then
			save <= INIT;
			else save <= save + 1;
		end if;
	end process;
	
	INS_A <= save;

end Behavioral;

