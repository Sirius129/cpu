----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:35:22 05/17/2019 
-- Design Name: 
-- Module Name:    LC - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LC is
	 Generic (TL : natural);
    Port ( OP : in  STD_LOGIC_VECTOR(7 downto 0);
           W : out  STD_LOGIC);
end LC;

architecture Behavioral of LC is

begin
	--MEM LC
	W <= '1' when ((OP = x"01" --ADD
				  or  OP = x"02" --MUL 
				  or  OP = x"03" --SOU
				  or  OP = x"04" --DIV
				  or  OP = x"05" --COP
				  or  OP = x"06" --AFC
				  or  OP = x"07" --LOAD
				  or  OP = x"09" --EQU
				  or  OP = x"0A" --INF
				  or  OP = x"0B" --INFE
				  or  OP = x"0C" --SUP
				  or  OP = x"0D" --SUPE
				  )and TL = 1)
				else '0' when TL = 1
				
   --RE LC
	else '1' when ((OP = x"01" --ADD
				  or  OP = x"02" --MUL 
				  or  OP = x"03" --SOU
				  or  OP = x"04" --DIV
				  or  OP = x"05" --COP
				  or  OP = x"06" --AFC
				  or  OP = x"07" --LOAD
				  or  OP = x"09" --EQU
				  or  OP = x"0A" --INF
				  or  OP = x"0B" --INFE
				  or  OP = x"0C" --SUP
				  or  OP = x"0D" --SUPE
				  or  OP = x"0E" --JMP
				  or  OP = x"0F" --JMPC
				  )and TL = 2)
				else '0' when TL = 2;

end Behavioral;

