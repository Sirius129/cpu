--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:48:17 05/27/2019
-- Design Name:   
-- Module Name:   /home/tallaa/Documents/Parser/projet_cpu/IP_TEST.vhd
-- Project Name:  CPU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: IP
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY IP_TEST IS
END IP_TEST;
 
ARCHITECTURE behavior OF IP_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT IP
    PORT(
         INIT : IN  std_logic_vector(15 downto 0);
         CLK : IN  std_logic;
         W : IN  std_logic;
         INS_A : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal INIT : std_logic_vector(15 downto 0) := (others => '0');
   signal CLK : std_logic := '0';
   signal W : std_logic := '0';

 	--Outputs
   signal INS_A : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 100 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: IP PORT MAP (
          INIT => INIT,
          CLK => CLK,
          W => W,
          INS_A => INS_A
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus 
	INIT <= x"0000";
	W <= '1', '0' after 500 ns;

END;
