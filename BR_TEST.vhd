--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:18:43 05/29/2019
-- Design Name:   
-- Module Name:   /home/tallaa/Documents/Parser/projet_cpu/BR_TEST.vhd
-- Project Name:  CPU
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Banc_de_Registres
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY BR_TEST IS
END BR_TEST;
 
ARCHITECTURE behavior OF BR_TEST IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Banc_de_Registres
    PORT(
         CLK : IN  std_logic;
         W : IN  std_logic;
         ATW : IN  std_logic_vector(7 downto 0);
         DATA : IN  std_logic_vector(7 downto 0);
         ATA : IN  std_logic_vector(7 downto 0);
         QA : OUT  std_logic_vector(7 downto 0);
         ATB : IN  std_logic_vector(7 downto 0);
         QB : OUT  std_logic_vector(7 downto 0);
         RST : IN  std_logic;
         R1 : OUT  std_logic_vector(7 downto 0);
         R2 : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal W : std_logic := '0';
   signal ATW : std_logic_vector(7 downto 0) := (others => '0');
   signal DATA : std_logic_vector(7 downto 0) := (others => '0');
   signal ATA : std_logic_vector(7 downto 0) := (others => '0');
   signal ATB : std_logic_vector(7 downto 0) := (others => '0');
   signal RST : std_logic := '0';

 	--Outputs
   signal QA : std_logic_vector(7 downto 0);
   signal QB : std_logic_vector(7 downto 0);
   signal R1 : std_logic_vector(7 downto 0);
   signal R2 : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Banc_de_Registres PORT MAP (
          CLK => CLK,
          W => W,
          ATW => ATW,
          DATA => DATA,
          ATA => ATA,
          QA => QA,
          ATB => ATB,
          QB => QB,
          RST => RST,
          R1 => R1,
          R2 => R2
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus 
	ATW <= x"01", x"02" after 200 ns, x"03" after 400 ns;
	W <= '1';
	DATA <= x"FF", x"33" after 200 ns, x"77" after 400 ns;
	RST <= '1';

END;
